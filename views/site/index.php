<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\GetTransactionFormModel */

$this->title = 'BCA Parser';
?>

<div class="col-md-12">
    <div class="col-md-4 col-md-offset-4" style="margin-top:50px;margin-bottom:20px;">
        <h1 class="text-center">BCA PARSER</h1>
    </div>
</div>
<div class="col-md-12">
    <div class="col-md-3">
        <div class="panel panel-primary ">
            <div class="panel-heading"><h3 class="panel-title">Login BCA</h3></div>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableClientValidation' => false,
            ]); ?>
            <div class="panel-body">
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?php if (!empty($model->firstErrors['formError'])): ?>
                    <small>
                        <div class="text-danger"><?= $model->firstErrors['formError'] ?></div>
                    </small>
                <?php endif ?>
            </div>
            <div class="panel-footer">
                <?= Html::submitButton('Process!', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>


    <div class="col-md-9">
        <div class="panel panel-primary ">
            <div class="panel-heading"><h3 class="panel-title">Result</h3></div>
            <div class="panel-body">
                <?php if ($model->isProcessed): ?>
                    <p><strong>Owner : <?= $model->owner; ?></strong></p>
                    <p><strong>Acc No. : <?= $model->accountNo; ?></strong></p>
                    <p><strong>Balance : Rp <?= number_format($model->balance, 0); ?></strong></p>

                    <?= GridView::widget([
                        'dataProvider' => $model->getProvider(),
                        'showPageSummary' => true,
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn'],
                            [
                                'attribute' => 'transactionDate',
                                'vAlign' => 'middle',
                                'value' => function ($data) {
                                    if ($data['transactionDate'] != "PEND") {
                                        return date('D, d M Y', strtotime($data['transactionDate']));
                                    } else {
                                        return $data['transactionDate'];
                                    }
                                },
                                'contentOptions' => [
                                    'style' => 'width:100px'
                                ],
                                'group' => true,
                                'groupedRow' => true,
                                'groupOddCssClass' => 'kv-grouped-row',
                                'groupEvenCssClass' => 'kv-grouped-row',
                            ],
                            [
                                'attribute' => 'category',
                                'vAlign' => 'middle',
                            ],
                            [
                                'attribute' => 'detail',
                                'vAlign' => 'middle',
                            ],
                            [
                                'attribute' => 'type',
                                'vAlign' => 'middle',
                                'contentOptions' => [
                                    'style' => 'width:50px'
                                ],
                            ],
                            [
                                'attribute' => 'amountDB',
                                'vAlign' => 'middle',
                                'label' => 'Amount DB',
                                'format' => ['decimal', 2],
                                'hAlign' => 'right',
                                'contentOptions' => [
                                    'style' => 'width:120px'
                                ],
                                'pageSummary' => true
                            ],
                            [
                                'attribute' => 'amountCR',
                                'vAlign' => 'middle',
                                'label' => 'Amount CR',
                                'format' => ['decimal', 2],
                                'hAlign' => 'right',
                                'contentOptions' => [
                                    'style' => 'width:120px'
                                ],
                                'pageSummary' => true
                            ],
                        ],
                    ]);
                    ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>