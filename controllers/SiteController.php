<?php

namespace app\controllers;

use app\models\GetTransactionFormModel;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

        $model = new GetTransactionFormModel();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->process();
            }
        }

        return $this->render('index', ['model' => $model]);
    }

}
