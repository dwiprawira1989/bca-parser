<?php

namespace app\modules\api\controllers;

use app\models\GetTransactionFormModel;
use Yii;
use yii\rest\Controller;

class BcaController extends Controller
{

    public function actionIndex()
    {
        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');
        $model = new GetTransactionFormModel();
        $model->username = $username;
        $model->password = $password;
        if ($model->validate() && $model->process()) {
            return [
                'status' => 'success',
                'accountNo' => $model->accountNo,
                'accountOwner' => $model->owner,
                'balance' => $model->balance,
                'transactions' => $model->transactions
            ];
        } else {
            $errorMessage = [];
            foreach ($model->errors as $error) {
                $errorMessage[] = $error;
            }
            return [
                'status' => 'failed',
                'errorMessage' => $errorMessage
            ];
        }
    }

}