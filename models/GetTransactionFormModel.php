<?php
namespace app\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Created by PhpStorm.
 * User: Dwi Prawira
 * Date: 4/7/2016
 * Time: 5:30 PM
 */
class GetTransactionFormModel extends Model
{
    public $username;
    public $password;
    public $formError;

    public $balance;
    public $transactions;
    public $owner;
    public $accountNo;
    public $isProcessed = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
        ];
    }

    public function process()
    {
        $parser = new BCAParser($this->username, $this->password);
        $this->username = "";
        $this->password = "";
        if (empty($parser->errorMessage)) {
            $this->transactions = $parser->transactions;
            $this->balance = $parser->balance;
            $this->accountNo = $parser->accountNo;
            $this->owner = $parser->accountOwner;
            $this->isProcessed = true;
            return true;
        } else {
            $this->addError("formError", $parser->errorMessage);
            $this->isProcessed = false;
            return false;
        }
    }

    public function getProvider()
    {
        $provider = new ArrayDataProvider([
            'key' => 'ordinal',
            'allModels' => $this->transactions,
            'pagination' => false,
            'sort' => [
                'attributes' => ['ordinal'],
                'defaultOrder' => ['ordinal' => SORT_DESC]
            ],
        ]);
        return $provider;
    }
}